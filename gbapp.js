var path = require("path")
var express = require("express")
var logger = require("morgan")
var bodyParser = require("body-parser")

var app = express()  // make express app
var server = require('http').createServer(app) // inject app into the server

// 1 set up the view engine
app.set("views", path.resolve(__dirname, "views")) // path to views
app.set("view engine", "ejs") // specify our view engine
app.use(express.static(__dirname+"/assets"))

// 2 manage our entries
var entries = []
app.locals.entries = entries // now entries can be accessed in .ejs files

// 3 set up the logger
app.use(logger("dev"))     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }))

app.get("/",(req,res)=>{
  res.sendFile(__dirname+"/assets/index.html")
})

app.get("/about",(req,res)=>{
  res.sendFile(__dirname+"/assets/about.html")
})

app.get("/contact",(req,res)=>{
  res.sendFile(__dirname+"/assets/contact.html")
})

// 4 GETS
app.get("/guestbook", function (request, response) {
  response.render("index")
})
app.get("/new-entry", function (request, response) {
  response.render("new-entry")
})
// 5 handle an http POST request to the new-entry URI 
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.")
    return
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  })
  // '/' is default path
  response.redirect("/guestbook")  // where to go next? Let's go to the home page :)
})
// POSTS
// 404 
// if we get a 404 status, render our 404.ejs view
app.use(function (request, response) {
  response.status(404).render("404")
})
// res.write("<body><h1>Hello World!!</h1></body></html>")


// Listen for an application request on port 8081
server.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/')
})